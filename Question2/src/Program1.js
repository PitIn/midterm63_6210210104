
import { Link
} from 'react-router-dom';


import {useState} from 'react';
import './App.css';


function Program1() {
    const [userA, setUserA] = useState("");
    const [userB, setUserB] = useState("");
    const [userC, setUserC] = useState("");
    const [result, setResult] = useState("");
    const [plus, setSum] = useState("");

    function hello() {
        setSum(userA + userB + userC);
        if (plus < 6) {
            setResult("แย่");
        }
        else if (plus >= 5 && plus <= 10) {
            setResult("ปานกลาง");
        }
        else if (plus >= 11 && plus <= 15) {
            setResult("ดีมาก");
        }
        else {
            setResult("เอาเลขอะไรใส่เข้าไปเนี่ย");
        }
    }
    return (
        <div>
            <div style={{ TextAlign: 'center' }}>
                <h1> ยินดีต้อนรับสู่เว็บรีวิวเกม </h1>
                <Link to="/Program2">
                    <h2> รีวิวเกม </h2>
                </Link>
            </div>
            <center><h1> คะแนนรีวิวเว็บ </h1></center>
                <p>ระดับคะแนน 0 น้อยสุด 5มากที่สุด</p>
                <p>ความเหมาะสมของเนื้อหา (0-5):</p>
                <input type="Text" value={userA}
                    onChange={
                        (e) => setUserA(e.target.value)
                    }
                />

                <p>ดีไซด์เว็บ (0-5) : </p><input type="Text"
                    value={userB}
                    onChange={
                        (e) => setUserB(e.target.value)
                    }
                />
                <p>ความสะดวกในการใช้งานเว็บ (0-5)</p> : <input type="Text"
                    value={userC}
                    onChange={
                        (e) => setUserC(e.target.value)
                    }
                />
                <button onClick={() => hello()}>
                    คะแนน
                </button>
                {
                    (plus != "") &&
                    (
                        <h2>คะแนนรีวิวของคุณ: {result} </h2>
                    )
                }
                {
                    (plus != "") &&
                    (
                        <h2> {plus} </h2>
                    )
                }        
        </div>
    );
}

export default Program1;
