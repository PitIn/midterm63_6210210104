import './App.css'
import Homepage from './Homepage';
import Program1 from './Program1';
import Program2 from './Program2';

import {BrowserRouter as Router, 
        Route,Switch,Link,
} from 'react-router-dom';



function App(){
  return(
    <Router>
      <AppBar>
        <Link to ="/">Homepage </Link> 
        <Link to ="/Program1"> /Program1</Link> 
        <Link to ="/Program2"> /Program2</Link>
      </AppBar>
    <div style={{marginTop : 80}}>
      <Switch>
          <Router exact path="/">
            <Homepage />
          </Router>
          <Router path="/Program1">
            <Program1 />
          </Router>
          <Router path="/Program2">
            <Program2 />
          </Router>
        </Switch>
      </div>
      </Router>
  );    
}
    

export default App;